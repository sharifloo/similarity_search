# fashion similarity


the aim is to carry out fashion searches by the query image and attribute manipulation,e.g. 
replacing long sleeves attribute of a dress to sleeveless. 

attributes are represented in two groups : 
1. general attributes (category , gender etc.) 
2. special attributes ( sleeve length , collar etc. ) 
special attributes are more suitable for the attribute manipulation thus conducting searches.
 their features are some texts.

## Part Extraction
![](./images/part_learning.png)

for the learning part they used alexnet for part extraction.   

![](./images/alexnet_part_extraction.png)

![](./images/part_based_training.png)

they used this feedback method

![](./images/feedback_method.png)
